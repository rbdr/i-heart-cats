I-heart-Cats
============

A platforming game with cats :3


## Xbox-Controller References ##

### Axes ###
  01: ls-horizontal
  02: ls-vertical
  03: rs-horizontal
  04: rs-vertical
  05: lt
  06: rt

### Buttons ###
  01: up
  02: down
  03: left
  04: right
  05: start
  06: back
  07: lsb
  08: rsb
  09: lb
  10: rb
  11: guide
  12: a
  13: b
  14: x
  15: y
