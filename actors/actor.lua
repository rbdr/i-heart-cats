Actor = class('Actor')

function Actor:initialize(x, y, image)

  --set properties with the constructor, with defaults
  self.x = x or 0
  self.y = y or 0
  self.image = love.graphics.newImage(image or "sprites/static/cat.png")
  self.name = "Generic Actor"

  self.scale = 2

  self.collide = true
  self.ai = nil
  self.friendly = true
  self.fps = 12
  self.immortal = false
  self.life = 10

  --bounding box
  local box_width = self.image:getWidth() * self.scale
  local box_height = self.image:getHeight() * self.scale
  self.box = {}
  self.box.top = math.floor(box_height/2)
  self.box.bottom = math.floor(box_height/2)
  self.box.left = math.floor(box_width/2)
  self.box.right = math.floor(box_width/2)

  --horizontal physics variables
  self.max_x_vel = 3      -- Maximum Velocity
  self.x_acc = 3          -- Acceleration
  self.x_decc = 2         -- Decceleration
  self.x_vel = 0          -- Current Velocity
  self.walk_time = 0      -- Total Walk time
  self.stop_time = 0      -- Total Stop time
  self.dir = 'right'      -- Current direction.

  --vertical physics variables
  self.max_y_vel = 32     -- Maximum Air Velocity
  self.gravity = 3        -- Gravity
  self.y_vel = 0          -- Current Air Velocity
  self.fall_time = 0      -- Time falling

end

function Actor:draw()
  love.graphics.draw(self.image, self.x, self.y, 0, self.scale, self.scale)
end

function Actor:draw_bounding_box()
  love.graphics.setColor(255, 0, 0, 128)
  love.graphics.rectangle("line", self:real_x()-self:box_side("left"), self:real_y()-self:box_side("top"), self:box_width(), self:box_height())
  love.graphics.setColor(255,255,255,255)
end

function Actor:update(dt)
  --Most of the actor code goes here.
  self:physics(dt)
end

function Actor:physics(dt)
  --move in x
  self.x = math.floor(self.x + self.x_vel)
  self:unstick("x")

  --move in y
  self:fall(dt)
  self.y = math.floor(self.y + self.y_vel)
  self:unstick("y")
end

function Actor:get_friction()

  local tiles = mapsys.map.tileLayers["Tiles"].tileData
  local friction = 1

    --get the tiles for this direction
    local tile_x1 = self:get_tile("x1", self.x)
    local tile_x2 = self:get_tile("x2", self.x)
    local tile_y2 = self:get_tile("y2", self.y)

    --loop from x1 to x2, and check with y2
    for i=tile_x1,tile_x2,1 do
      if tiles:get(i, tile_y2) then
        if tiles:get(i, tile_y2).properties.friction then
          friction = tonumber(tiles:get(i, tile_y2).properties.friction)
        end
      end
    end

  return friction
end

function Actor:accelerate(dt)
  self.walk_time = self.walk_time + dt
  self.stop_time = 0

  if self.dir == "right" then
    if self:can_pass("right") then
      if self.x_vel > self.max_x_vel then
        self.x_vel = self.max_x_vel
      else
        self.x_vel = self.x_vel + (self.x_acc * self.walk_time * self:get_friction())
      end
    else
      self.x_vel = 0
      self.walk_time = 0
    end
  else
    if self:can_pass("left") then
      if self.x_vel < -self.max_x_vel then
        self.x_vel = -self.max_x_vel
      else
        self.x_vel = self.x_vel - (self.x_acc * self.walk_time * self:get_friction())
      end
    else
      self.x_vel = 0
      self.walk_time = 0
    end
  end
end

function Actor:deccelerate(dt)
  self.stop_time = self.stop_time + dt
  self.walk_time = 0


  if self.x_vel ~= 0 then
    if self.x_vel > 0 then
      if self.x_vel > self.max_x_vel then
        self.x_vel = self.max_x_vel
      end

      self.x_vel = self.x_vel - (self.x_decc * self.stop_time * self:get_friction())

      if self.x_vel < 0 then
        self.x_vel = 0
      end
    elseif self.x_vel < 0 then
      if self.x_vel < -self.max_x_vel then
        self.x_vel = -self.max_x_vel
      end

      self.x_vel = self.x_vel + (self.x_decc * self.stop_time * self:get_friction())

      if self.x_vel > 0 then
        self.x_vel = 0
      end
    else
      self.x_vel = 0
    end
  end
end

function Actor:fall(dt)
  if self:can_pass("down") then
    self.fall_time = self.fall_time + dt
    self.y_vel = self.y_vel + self.gravity * self.fall_time

    if self.y_vel > self.max_y_vel then
      self.y_vel = self.max_y_vel
    end
  else
    self.fall_time = 0
    self.y_vel = 0
  end
end

function Actor:unstick(context)
  if context == "y" then
    while not self:can_pass("down", self.x, self.y-1) do
      self.y = self.y-1
    end
  end

  if context == "x" then
    while not self:can_pass("right") do
      if self.x_vel > 0 then
        self.x_vel = 1
      end
      self.x = self.x-1
    end

    while not self:can_pass("left") do
      self.x = self.x+1
      if self.x_vel < 0 then
        self.x_vel = -1
      end
    end
  end
end

function Actor:real_x()
  return math.floor(self.x - mapsys.tx)
end

function Actor:real_y()
  return math.floor(self.y - mapsys.ty)
end

function Actor:get_tile(side, val)
  if side == "x1" then
    return math.floor( (val - self:box_side("left")) / mapsys.map.tileWidth / mapsys.scale)
  end

  if side == "x2" then
    return math.floor( (val + self:box_side("right")) / mapsys.map.tileWidth / mapsys.scale)
  end

  if side == "y1" then
    return math.floor( (val - self:box_side("top")) / mapsys.map.tileHeight / mapsys.scale)
  end

  if side == "y2" then
    return math.floor( (val + self:box_side("bottom")) / mapsys.map.tileHeight / mapsys.scale)
  end
end

function Actor:can_pass(direction, x, y)

  x = x or self.x
  y = y or self.y

  --the padding to check
  padding = 2

  local tiles = mapsys.map.tileLayers["Collisions"].tileData

  if direction == "up" then

    --get the tiles for this direction
    local tile_x1 = self:get_tile("x1", x+padding)
    local tile_x2 = self:get_tile("x2", x-padding)
    local tile_y1 = self:get_tile("y1", y)

    --loop from x1 to x2, and check with y1
    for i=tile_x1,tile_x2,1 do
      if tiles:get(i, tile_y1) then
        if tiles:get(i, tile_y1).properties.passable == 0 then
          return false
        end
      end
    end

    --test edge of roof
    if y <= 0 then
      return false
    end
  end

  if direction == "down" then

    --get the tiles for this direction
    local tile_x1 = self:get_tile("x1", x+padding)
    local tile_x2 = self:get_tile("x2", x-padding)
    local tile_y2 = self:get_tile("y2", y)

    --loop from x1 to x2, and check with y2
    for i=tile_x1,tile_x2,1 do
      if tiles:get(i, tile_y2) then
        if tiles:get(i, tile_y2).properties.passable == 0 then
          return false
        end
      end
    end

    --test edge of floor
    if y >= mapsys.map.height * mapsys.map.tileHeight * mapsys.scale then
      return false
    end
  end

  if direction == "left" then

    --get the tiles for this direction
    local tile_x1 = self:get_tile("x1", x)
    local tile_y1 = self:get_tile("y1", y+padding)
    local tile_y2 = self:get_tile("y2", y-padding)

    --loop from y1 to y2, and check with x1
    for i=tile_y1,tile_y2,1 do
      if tiles:get(tile_x1, i) then
        if tiles:get(tile_x1, i).properties.passable == 0 then
          return false
        end
      end
    end

    --test left wall
    if x <= 0 then
      return false
    end
  end

  if direction == "right" then

    --get the tiles for this direction
    local tile_x2 = self:get_tile("x2", x)
    local tile_y1 = self:get_tile("y1", y+padding)
    local tile_y2 = self:get_tile("y2", y-padding)

    --loop from y1 to y2, and check with x2
    for i=tile_y1,tile_y2,1 do
      if tiles:get(tile_x2, i) then
        if tiles:get(tile_x2, i).properties.passable == 0 then
          return false
        end
      end
    end

    --test edge of roof
    if x >= mapsys.map.width * mapsys.map.tileWidth * mapsys.scale then
      return false
    end
  end

  return true

end

function Actor:flip(dir)
  if dir == "right" then
      self.x = self.x + math.abs(self.box.right - self.box.left)
  end

  if dir == "left" then
      self.x = self.x - math.abs(self.box.right - self.box.left)
  end
end

function Actor:box_side(side)
  if self.dir == "left" then
    if side == "left" then
      return self.box["right"]
    elseif side == "right" then
      return self.box["left"]
    else
      return self.box[side]
    end
  else
    return self.box[side]
  end
end

function Actor:box_width()
  return self.box.right+self.box.left
end

function Actor:box_height()
  return self.box.top+self.box.bottom
end
