Player = class('Player', Actor)

function Player:initialize(x, y, image)
  Actor.initialize(self, x, y, image)

  self.name = "Player"

  --define the individual sprite width and height
  self.sprite_width = 64
  self.sprite_height = 64

  self:create_animation()

  --custom bounding box
  self.box.top = 20
  self.box.bottom = 64
  self.box.left = 18
  self.box.right = 10

  self.max_x_vel = 5      -- Maximum Velocity

  local particleimage = love.graphics.newImage('sprites/static/heart.png')

  self.particle = love.graphics.newParticleSystem(particleimage, 20)
  self.particle:setEmissionRate(10)
  self.particle:setSpeed(20, 50)
  self.particle:setGravity(0, -250)
  self.particle:setSizes(1.5, .5)
  self.particle:setColors(204, 0, 0, 255, 204, 0, 0, 0)
  self.particle:setPosition(400, 300)
  self.particle:setLifetime(1)
  self.particle:setParticleLife(1)
  self.particle:setDirection(-190)
  self.particle:setSpread(100)
  self.particle:setRadialAcceleration(100, 30)
  self.particle:stop()
end

function Player:create_animation()
  local g = anim8.newGrid(self.sprite_width, self.sprite_height, self.image:getWidth(), self.image:getHeight())
  self.animation = anim8.newAnimation('loop', g('2-7,1'), 0.1)
  self.idle = anim8.newAnimation('once', g('1,1'), 0.1)
end

function Player:draw()
  love.graphics.draw(self.particle, 0, 0)
  love.graphics.setColorMode('replace')
  if self.walk_time ~= 0 then
    if self.dir == "right" then
      self.animation:draw(self.image, self:real_x(), self:real_y(), 0, self.scale, self.scale, self.sprite_width/2, self.sprite_height/2)
    else
      self.animation:draw(self.image, self:real_x(), self:real_y(), 0, -self.scale, self.scale, self.sprite_width/2, self.sprite_height/2)
    end
  else
    if self.dir == "right" then
      self.idle:draw(self.image, self:real_x(), self:real_y(), 0, self.scale, self.scale, self.sprite_width/2, self.sprite_height/2)
    else
      self.idle:draw(self.image, self:real_x(), self:real_y(), 0, -self.scale, self.scale, self.sprite_width/2, self.sprite_height/2)
    end
  end

  --draw bounding box.
  self:draw_bounding_box()
end

function Player:update(dt)
  --regular keyboard / joypad control
  --TODO: Move these "right, left, etc." to a config file. Much laters though.
  --NOTE: Joystick buttons mapped to xbox controller


  if love.keyboard.isDown("z") or love.joystick.isDown(1,12) then
    print("should jump")
    --jump
  end

  if love.keyboard.isDown( "right" ) or love.joystick.getAxis(1,1) > 0.5  or love.joystick.isDown(1,4) then
    if self.dir == 'left' then
      self:flip('right')
      self.walk_time = dt
    end
    self.dir = 'right'
  end

  if love.keyboard.isDown( "left" ) or love.joystick.getAxis(1,1) < -0.5  or love.joystick.isDown(1,3) then
    if self.dir == 'right' then
      self:flip('left')
      self.walk_time = dt
    end
    self.dir = 'left'
  end

  if not love.keyboard.isDown( "right" ) and not love.keyboard.isDown( "left" )
     and not (love.joystick.getAxis(1,1) > 0.5) and not (love.joystick.getAxis(1,1) < -0.5)
     and not love.joystick.isDown(1,3) and not love.joystick.isDown(1,4) then
    if self.x_vel ~= 0 then
      self:deccelerate(dt)
    end
  else
    self.stop_time = 0
    self:accelerate(dt)
  end

  self.animation:update(dt)
  self.particle:setPosition(self:real_x(), self:real_y())
  self.particle:start()
  self.particle:update(dt)

  self:physics(dt)
  self:centermap()
end

function Player:centermap()

  --Set the center of the map as the center of the screen
  mapsys.tx = self.x - love.graphics.getWidth()/2
  mapsys.ty = self.y - love.graphics.getHeight()/2

  --Stop centering if we're at the edge
  if mapsys.tx < 0 then
    mapsys.tx = 0
  elseif mapsys.tx + love.graphics.getWidth() >= mapsys.map.width * mapsys.map.tileWidth * mapsys.scale then
    mapsys.tx = mapsys.map.width * mapsys.map.tileWidth * mapsys.scale - love.graphics.getWidth()
  end

  if mapsys.ty < 0 then
    mapsys.ty = 0
  elseif mapsys.ty + love.graphics.getHeight() >= mapsys.map.height * mapsys.map.tileHeight * mapsys.scale then
    mapsys.ty = mapsys.map.height * mapsys.map.tileHeight * mapsys.scale - love.graphics.getHeight()
  end
end
