--NOTE: this cat is hella buggy. No clue why and I haven't checked
--but first: y is not centered (I think, maybe it's just a giant gap)
--second: the whole acceleration is weird.
--I haven't touched this since the 90s, but it's crazy.

Cat = class('Cat', Actor)

function Cat:initialize(x,y,image)
  Actor.initialize(self, x, y, image)

  self.name = "Cat"

  --define the individual sprite width and height
  self.sprite_width = 64
  self.sprite_height = 64

  self:create_animation()

  --Redefine bounding box.
  self.box.top = -self.sprite_height / 2
  self.box.bottom = self.sprite_height
  self.box.left = 24
  self.box.right = 24

  --horizontal_physics
  self.max_x_vel = 3
end

function Cat:create_animation()
  local g = anim8.newGrid(self.sprite_width,self.sprite_height, self.image:getWidth(), self.image:getHeight())
  self.animation = anim8.newAnimation('loop', g(1,1, 2,1, 1,1, 3,1), 0.1)
  self.idle = anim8.newAnimation('once', g('1,1'), 0.1)
end

function Cat:draw()
  if self.dir == "right" then
    self.animation:draw(self.image, self:real_x(), self:real_y(), 0, self.scale, self.scale, self.sprite_width/2, self.sprite_height/2)
  else
    self.animation:draw(self.image, self:real_x(), self:real_y(), 0, -self.scale, self.scale, self.sprite_width/2, self.sprite_height/2)
  end
  self:draw_bounding_box()
end

function Cat:update(dt)
  if not self:can_pass("right", self.x+1, self.y) and self.dir == "right" then
    self:flip('left')
    self.dir = "left"
    self.walk_time = dt
  end

  if not self:can_pass("left", self.x-1, self.y) and self.dir == "left" then
    self:flip("right")
    self.dir = "right"
    self.walk_time = dt
  end

  self:accelerate(dt)
  self:physics(dt)
  self.animation:update(dt)
end
