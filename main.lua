require('lib/vendor/middleclass')
table.inspect = require('lib/vendor/inspect')
anim8 = require('lib/vendor/anim8')
loader = require('lib/vendor/AdvTiledLoader/loader')
require('lib/extra_math')

require('actors/actor')
require('actors/player')
require('actors/cat-test')

entities = {}

function love.load()

  --configuration (this should probably be its own file, um, TODO)
  min_dt = 1/30
  next_time = love.timer.getMicroTime()

  --set scale mode
  love.graphics.setDefaultImageFilter( 'nearest', 'nearest' )

  --map system loading
  loader.path = "maps/"
  mapsys = {map = loader.load("test.tmx"), tx = 0, ty = 0, scale = 2, padding = 0}
  mapsys.map.useSpriteBatch = true

  --don't draw the collision layer
  local i = mapsys.map:drawPosition( mapsys.map.tileLayers["Collisions"])
  table.remove(mapsys.map.drawList, i)

  --load stuff
  love.graphics.setBackgroundColor(255, 255, 255);

  --Don't add actors this way, use overwatch:move_in()
  player = Player:new(64, 64, 'sprites/anim/girl-64-64.png')
  cat = Cat:new(320,240, 'sprites/anim/cat-64-64.png')
end

function love.update(dt)
  --limit frames
  next_time = next_time + min_dt

  --Run the update code for every actor
  --An overwatch class could handle this.
  player:update(dt)
  cat:update(dt)
end

function love.draw()


  --map draw
  local ftx, fty = math.floor(mapsys.tx/mapsys.scale), math.floor(mapsys.ty/mapsys.scale)
  mapsys.map:autoDrawRange(-ftx, -fty, mapsys.scale, mapsys.padding)
  love.graphics.push()
  love.graphics.scale(mapsys.scale)
  love.graphics.translate(-ftx, -fty)
  mapsys.map:draw()
  love.graphics.pop()


  --player draw
  player:draw() --Delegate this to overwatch:draw()
  cat:draw()

  --Same as update, draw per actor.

  --DEBUG: show fps
  debug_message(love.timer.getFPS(), 10, 10)

  --limit the time
  local cur_time = love.timer.getMicroTime()
  if next_time <= cur_time then
    next_time = cur_time
    return
  end
  love.timer.sleep(next_time - cur_time)
end

function debug_message(message, x, y)
  love.graphics.setColorMode("modulate")
  love.graphics.setColor(0,0,0,255)
  love.graphics.print(message, x, y)
  love.graphics.setColorMode("replace")
end

function love.joystickpressed(joystick, button)
  --Handle joystick
end

function love.keypressed(key, unicode)
  --Handle keyboard
end

function love.quit()
  print("Goodbye!")
end
